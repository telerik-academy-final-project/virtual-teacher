create or replace table roles
(
    id int not null
        primary key,
    name varchar(50) not null
);

create or replace table topics
(
    id int auto_increment
        primary key,
    name varchar(50) not null,
    constraint topics_name_uindex
        unique (name)
);

create or replace table courses
(
    id int auto_increment
        primary key,
    title varchar(50) not null,
    topic_id int not null,
    description varchar(1000) not null,
    starting_date date null,
    constraint courses_title_uindex
        unique (title),
    constraint courses_topics_id_fk
        foreign key (topic_id) references topics (id)
);

create or replace table users
(
    id int auto_increment
        primary key,
    first_name varchar(20) not null,
    last_name varchar(20) not null,
    email varchar(50) not null,
    password varchar(50) not null,
    phone varchar(20) not null,
    img_path text not null
);

create or replace table assignments
(
    id int auto_increment
        primary key,
    assignment_path text not null,
    assignment_grade int null,
    user_id int null,
    constraint assignments_users_id_fk
        foreign key (user_id) references users (id)
);

create or replace table courses_ratings
(
    id int auto_increment
        primary key,
    user_id int not null,
    course_id int not null,
    rating double not null,
    constraint courses_ratings_courses_id_fk
        foreign key (course_id) references courses (id),
    constraint courses_ratings_users_id_fk
        foreign key (user_id) references users (id)
);

create or replace table lectures
(
    id int auto_increment
        primary key,
    title varchar(50) not null,
    description varchar(1000) not null,
    video_path text null,
    assignment_id int null,
    constraint lectures_assignment_id_uindex
        unique (assignment_id),
    constraint lectures_assignments_id_fk
        foreign key (assignment_id) references assignments (id)
);

create or replace table course_lectures
(
    course_id int not null,
    lecture_id int not null,
    constraint course_lectures_courses_id_fk
        foreign key (course_id) references courses (id),
    constraint course_lectures_lectures_id_fk
        foreign key (lecture_id) references lectures (id)
);

create or replace table users_courses
(
    user_id int not null,
    course_id int not null,
    constraint users_courses_courses_id_fk
        foreign key (course_id) references courses (id),
    constraint users_courses_users_id_fk
        foreign key (user_id) references users (id)
);

create or replace table users_roles
(
    user_id int not null,
    role_id int not null,
    constraint users_roles_roles_id_fk
        foreign key (role_id) references roles (id),
    constraint users_roles_users_id_fk
        foreign key (user_id) references users (id)
);

