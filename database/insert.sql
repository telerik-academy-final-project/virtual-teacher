insert into virtualteacher.roles(id, name)
VALUES(1, 'Administrator'),
      (2, 'Teacher'),
      (3, 'Student');


insert into virtualteacher.users(id, first_name, last_name, email, password, phone, img_path)
VALUES(1, 'Daniel', 'Filipov','dff_98@abv.bg','Password1!','0888888888',null),
      (2, 'Ivan', 'Kolev','ivankolev@abv.bg','Password2!','0888888889',null),
      (3, 'Preslav', 'Hristov','preslavhristov@abv.bg','Password3!','0888888810',null);

insert into virtualteacher.users_roles(user_id, role_id)
VALUES (1, 1),
       (2, 2),
       (3, 3);
