package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.Lecture;
import com.telerikacademy.web.virtualteacher.models.User;

import java.util.List;

public interface LectureService {
    List<Lecture> getAll();

    Lecture getById(int id);

    void delete(int id);

    void update(Lecture lecture, User user);

    void create(Lecture lecture, User user);
}
