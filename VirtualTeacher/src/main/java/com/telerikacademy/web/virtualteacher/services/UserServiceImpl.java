package com.telerikacademy.web.virtualteacher.services;


import com.telerikacademy.web.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.Course;
import com.telerikacademy.web.virtualteacher.models.User;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.CourseRepository;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.UserRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final CourseRepository courseRepository;

    public UserServiceImpl(UserRepository userRepository, CourseRepository courseRepository) {
        this.userRepository = userRepository;
        this.courseRepository = courseRepository;
    }


    @Override
    public List<User> getAll() {
        return this.userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return this.userRepository.getById(id);
    }

    @Override
    public void create(User user) {
        boolean duplicateExists = true;
        try {
            userRepository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        userRepository.create(user);
    }

    @Override
    public void update(User user) {

        User userToUpdate = userRepository.getById(user.getId());

        boolean duplicateEmailExists = true;
        try {
            if (!userToUpdate.getEmail().equals(user.getEmail())) {
                userRepository.getByEmail(user.getEmail());
            }
            if (userToUpdate.getEmail().equals(user.getEmail())) {
                duplicateEmailExists = false;
            }

        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        userRepository.update(user);
    }

    @Override
    public void delete(int id, User user) {
        userRepository.delete(id);
    }

    @Override
    public List<User> search(Optional<String> email, Optional<Integer> phone, User user) {
        return userRepository.search(email, phone);
    }

    @Override
    public void enroll(User user, int courseId) {
        User userToEnroll = userRepository.getById(user.getId());
        Course courseToAdd = courseRepository.getById(courseId);

        if (userToEnroll.getCourses().stream()
                .anyMatch(course -> course.getId() == courseToAdd.getId())) {
            throw new DuplicateEntityException("Course", "title", courseToAdd.getTitle());
        }

        userToEnroll.getCourses().add(courseToAdd);

        update(userToEnroll);
    }

    @Override
    public List<User> getAllTeachers() {
        return this.userRepository.getAllTeachers();
    }


    /*//boolean check = roleCheck(user, "student");
    private boolean roleCheck(User user, String roleName) {
        return user.getRoles().stream()
                .anyMatch(role -> role.getName().toLowerCase().contains(roleName));

    }*/

    @Override
    public User getByEmail(String email) {
        return this.userRepository.getByEmail(email);
    }
}
