package com.telerikacademy.web.virtualteacher.services.modelmappers;

import com.telerikacademy.web.virtualteacher.models.Lecture;
import com.telerikacademy.web.virtualteacher.models.dtos.LectureDto;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.LectureRepository;
import org.springframework.stereotype.Component;

@Component
public class LectureModelMapper {
    private final LectureRepository lectureRepository;
    //private final AssignmentRepository assignmentRepository;

    public LectureModelMapper(LectureRepository lectureRepository) {
        this.lectureRepository = lectureRepository;
    }

    public Lecture fromDto(LectureDto lectureDto) {
        Lecture lecture = new Lecture();
        dtoToObject(lectureDto, lecture);
        return lecture;
    }

    public Lecture fromDto(LectureDto lectureDto, int id) {
        Lecture lecture = lectureRepository.getById(id);
        dtoToObject(lectureDto, lecture);
        return lecture;
    }


    private void dtoToObject(LectureDto lectureDto, Lecture lecture) {
        lecture.setTitle(lectureDto.getTitle());
        lecture.setDescription(lectureDto.getDescription());
        lecture.setVideoPath(lectureDto.getVideoPath());
    }
}
