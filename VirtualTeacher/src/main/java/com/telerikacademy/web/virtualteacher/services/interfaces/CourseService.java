package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.Course;
import com.telerikacademy.web.virtualteacher.models.User;

import java.util.List;
import java.util.Optional;

public interface CourseService {

    List<Course> getAll();

    Course getById(int id);

    void delete(int id);

    void update(Course course, User user);

    void create(Course course, User user);

    List<Course> filter(Optional<String> courseName,
                        Optional<Integer> courseTopicId,
                        Optional<String> description);

    List<Course> sortByTitle();
}
