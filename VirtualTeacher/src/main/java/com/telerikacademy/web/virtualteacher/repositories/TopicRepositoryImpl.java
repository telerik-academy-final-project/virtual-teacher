package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.Topic;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.TopicRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TopicRepositoryImpl implements TopicRepository {

    private final SessionFactory sessionFactory;

    public TopicRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Topic> getAll(){
        try (Session session = sessionFactory.openSession()){
            Query<Topic> query =session.createQuery("from Topic", Topic.class);
            return query.list();
        }
    }

    @Override
    public Topic getById(int id){
        try (Session session = sessionFactory.openSession()) {
            Topic topic = session.get(Topic.class, id);
            if (topic == null) {
                throw new EntityNotFoundException("Topic", id);
            }
            return topic;
        }
    }

    @Override
    public Topic getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Topic topic = session.get(Topic.class, name);
            if (topic == null) {
                throw new EntityNotFoundException("Topic", name, "");
            }
            return topic;
        }
    }
}
