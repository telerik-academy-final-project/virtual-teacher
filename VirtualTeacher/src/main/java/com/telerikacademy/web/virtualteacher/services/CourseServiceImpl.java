package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.Course;
import com.telerikacademy.web.virtualteacher.models.User;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.CourseRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Component
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }


    public List<Course> getAll() {
        return courseRepository.getAll();
    }

    public Course getById(int id) {
        try {
            return courseRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        courseRepository.delete(id);
    }

    @Override
    public void update(Course course, User user) {
        if (!user.roleCheck(user, "teacher")) {
            throw new UnsupportedOperationException("Only teachers can update courses");
        }
        courseRepository.update(course);
    }

    @Override
    public void create(Course course, User user) {
        if (!user.roleCheck(user, "teacher")) {
            throw new UnsupportedOperationException("Only teachers can create courses");
        }
        courseRepository.create(course);
    }

    @Override
    public List<Course> filter(Optional<String> courseName, Optional<Integer> courseTopicId, Optional<String> description) {
        return courseRepository.filter(courseName, courseTopicId, description);
    }

    @Override
    public List<Course> sortByTitle() {
        return courseRepository.sortByTitle();
    }
}
