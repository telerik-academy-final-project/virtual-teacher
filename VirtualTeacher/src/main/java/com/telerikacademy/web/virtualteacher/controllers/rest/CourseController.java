package com.telerikacademy.web.virtualteacher.controllers.rest;

import com.telerikacademy.web.virtualteacher.controllers.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.Course;
import com.telerikacademy.web.virtualteacher.models.User;
import com.telerikacademy.web.virtualteacher.models.dtos.CourseDto;
import com.telerikacademy.web.virtualteacher.services.interfaces.CourseService;
import com.telerikacademy.web.virtualteacher.services.modelmappers.CourseModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/courses")
public class CourseController {
    private final CourseService courseService;
    private final CourseModelMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CourseController(CourseService courseService, CourseModelMapper mapper, AuthenticationHelper authenticationHelper) {
        this.courseService = courseService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Course> getAll() {
        return courseService.getAll();
    }

    @GetMapping("/{id}")
    public Course getById(@PathVariable int id) {
        try {
            return courseService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@Valid @RequestBody CourseDto courseDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            var course = mapper.fromDto(courseDto);
            courseService.create(course, user);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id, @Valid @RequestBody CourseDto courseDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Course course = mapper.fromDto(courseDto, id);
            courseService.update(course, user);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            courseService.delete(id);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Course> filter(@RequestParam(required = false) Optional<String> courseName,
                               @RequestParam(required = false) Optional<Integer> courseTopicId,
                               @RequestParam(required = false) Optional<String> description) {

        return courseService.filter(courseName, courseTopicId, description);
    }

    @GetMapping("/sort")
    public List<Course> filter() {
        return courseService.sortByTitle();
    }
}
