package com.telerikacademy.web.virtualteacher.models;

import javax.persistence.*;

@Entity
@Table(name = "assignments")
public class Assignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name ="assignment_path")
    private String path;

    @Column(name = "assignment_grade")
    private int grade;

    @Column(name = "user_id")
    private int userId;

    public Assignment() {
    }

    public Assignment(int id, String path, int grade, int userId) {
        setId(id);
        setPath(path);
        setGrade(grade);
        setUserId(userId);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
