package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.Role;

import java.util.List;

public interface RoleService {

    List<Role> getAll();

    Role getById(int id);

}
