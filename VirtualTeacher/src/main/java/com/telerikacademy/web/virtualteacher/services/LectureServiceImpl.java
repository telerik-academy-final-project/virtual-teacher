package com.telerikacademy.web.virtualteacher.services;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.Lecture;
import com.telerikacademy.web.virtualteacher.models.User;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.LectureRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Component
public class LectureServiceImpl implements LectureService {
    private final LectureRepository lectureRepository;

    @Autowired
    public LectureServiceImpl(LectureRepository lectureRepository) {
        this.lectureRepository = lectureRepository;
    }

    @Override
    public List<Lecture> getAll() {
        return lectureRepository.getAll();
    }

    @Override
    public Lecture getById(int id) {
        try {
            return lectureRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    public void delete(int id) {
        lectureRepository.delete(id);
    }

    public void update(Lecture lecture, User user) {
        if (!user.roleCheck(user, "teacher")) {
            throw new UnsupportedOperationException("Only teachers can update courses");
        }
        lectureRepository.update(lecture);
    }

    public void create(Lecture lecture, User user) {
        if (!user.roleCheck(user, "teacher")) {
            throw new UnsupportedOperationException("Only teachers can create courses");
        }
        lectureRepository.create(lecture);
    }
}
