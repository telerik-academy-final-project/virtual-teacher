package com.telerikacademy.web.virtualteacher.models.dtos;

import javax.validation.constraints.*;

public class ApprovalUserTeacherDto {

    @Email(message = "Invalid email address")
    private String email;

    //@Min(value = 0)
    //@Max(value = 1)
    @NotNull
    private boolean approved;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
