package com.telerikacademy.web.virtualteacher.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;

    @Column(name = "description")
    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "course_lectures",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "lecture_id"))
    private List<Lecture> lectures;

    @Column(name = "starting_date")
    private LocalDate startingDate;

    //How to store rating
//    @OneToMany
//    @JoinTable(name = "users_rating",
//                 = @JoinColumn(name = "rating_id") )
//
//    private Set<Integer> rating;

    public Course() {
    }

    public Course(int id, String title, Topic topic, String description, List<Lecture> lectures, LocalDate startingDate) {
        setId(id);
        setTitle(title);
        setTopic(topic);
        setDescription(description);
        setLectures(lectures);
        setStartingDate(startingDate);
        //rating = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Lecture> getLectures() {
        return lectures;
    }

    public void setLectures(List<Lecture> setOfLectures) {
        this.lectures = setOfLectures;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }
}