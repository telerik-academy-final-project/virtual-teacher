package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.RequiredRegistrationInformationException;
import com.telerikacademy.web.virtualteacher.models.User;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return result.get(0);
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            if (user.getFirstName() == null &&
                    user.getLastName() == null &&
                    user.getEmail() == null &&                       //Todo duplicate #1
                    user.getPassword() == null &&
                    user.getPhone() == 0) {
                throw new RequiredRegistrationInformationException();
            }
            session.save(user);
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            if (user.getFirstName() == null &&
                    user.getLastName() == null &&
                    user.getEmail() == null &&                       //Todo duplicate #1
                    user.getPassword() == null &&
                    user.getPhone() == 0) {
                throw new RequiredRegistrationInformationException();
            }
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> search(Optional<String> email, Optional<Integer> phone) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createNativeQuery("select * from virtualteacher.users " +
                    "where (:email is null or email like concat('%',:email,'%')) " +
                    "and (:phone is null or phone like concat('%',:phone,'%')) ", User.class);
            query.setParameter("email", email.orElse(null));
            query.setParameter("phone", phone.orElse(null));
            if (query.list().size() == 0) {
                return new ArrayList<>();
            }
            return query.list();
        }

    }

    @Override
    public List<User> getAllTeachers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User order by approved asc ", User.class);

            return query.list().stream().filter(user -> user.roleCheck(user,"teacher"))
                    .collect(Collectors.toList());
        }
    }
}
