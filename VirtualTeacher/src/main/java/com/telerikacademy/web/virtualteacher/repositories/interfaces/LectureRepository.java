package com.telerikacademy.web.virtualteacher.repositories.interfaces;

import com.telerikacademy.web.virtualteacher.models.Lecture;

import java.util.List;

public interface LectureRepository {
    List<Lecture> getAll();

    Lecture getById(int id);

    Lecture getByName(String name);

    void delete(int id);

    void update(Lecture lecture);

    void create(Lecture lecture);
}
