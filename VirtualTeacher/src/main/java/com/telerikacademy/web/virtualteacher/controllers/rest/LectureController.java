package com.telerikacademy.web.virtualteacher.controllers.rest;

import com.telerikacademy.web.virtualteacher.controllers.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.Lecture;
import com.telerikacademy.web.virtualteacher.models.User;
import com.telerikacademy.web.virtualteacher.models.dtos.LectureDto;
import com.telerikacademy.web.virtualteacher.services.interfaces.LectureService;
import com.telerikacademy.web.virtualteacher.services.modelmappers.LectureModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/lectures")
public class LectureController {
    private final LectureService lectureService;
    private final LectureModelMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public LectureController(LectureService lectureService, LectureModelMapper mapper, AuthenticationHelper authenticationHelper) {
        this.lectureService = lectureService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Lecture> getAll() {
        return lectureService.getAll();
    }

    @GetMapping("/{id}")
    public Lecture getById(@PathVariable int id) {
        try {
            return lectureService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@Valid @RequestBody LectureDto lectureDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            var lecture = mapper.fromDto(lectureDto);
            lectureService.create(lecture, user);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id, @Valid @RequestBody LectureDto lectureDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Lecture lecture = mapper.fromDto(lectureDto, id);
            lectureService.update(lecture, user);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            lectureService.delete(id);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
