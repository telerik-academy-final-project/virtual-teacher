package com.telerikacademy.web.virtualteacher.repositories.interfaces;

import com.telerikacademy.web.virtualteacher.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getAll();

    User getById(int id);

    User getByEmail(String email);

    void create(User user);

    void update(User user);

    void delete(int id);

    List<User> search(Optional<String> email,
                      Optional<Integer> phone);

    List<User> getAllTeachers();
}
