package com.telerikacademy.web.virtualteacher.exceptions;

public class RequiredRegistrationInformationException extends RuntimeException{
    public RequiredRegistrationInformationException(){
        super("Users must have First and Last mame, email, password and phone!");
    }
}
