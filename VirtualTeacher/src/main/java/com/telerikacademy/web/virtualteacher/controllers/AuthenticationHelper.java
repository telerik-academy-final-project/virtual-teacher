package com.telerikacademy.web.virtualteacher.controllers;

import com.telerikacademy.web.virtualteacher.models.User;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;

@Component
public class AuthenticationHelper {
    public static final String AUTHORIZATION_HEADER_EMAIL = "email";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }


    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_EMAIL)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The requested resource requires authentication by providing email in the headers.");
        }

        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_EMAIL);
            return userService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid email.");
        }
    }

}
