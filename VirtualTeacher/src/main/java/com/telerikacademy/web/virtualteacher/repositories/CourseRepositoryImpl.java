package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.Course;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.CourseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CourseRepositoryImpl implements CourseRepository {
    SessionFactory sessionFactory;

    public CourseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Course> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery("from Course", Course.class);
            return query.list();
        }
    }

    @Override
    public Course getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Course course = session.get(Course.class, id);
            if (course == null) {
                throw new EntityNotFoundException("Course", id);
            }
            return course;
        }
    }

    @Override
    public Course getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Course course = session.get(Course.class, name);
            if (course == null) {
                throw new EntityNotFoundException("Course", name, "");
            }
            return course;
        }
    }

    @Override
    public void delete(int id) {
        Course courseToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(courseToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Course course) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(course);
            session.getTransaction().commit();
        }
    }

    @Override
    public void create(Course course) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(course);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Course> filter(Optional<String> courseName, Optional<Integer> courseTopicId, Optional<String> description) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query;
            query = session.createNativeQuery("select * from virtualteacher.courses " +
                    "where (:title is null or title=:title) " +
                    "and (:topic_id = 0 or topic_id=:topic_id)" +
                    "and (:description IS NULL or description=:description)", Course.class);
            query.setParameter("title", courseName.orElse(null));
            query.setParameter("topic_id", courseTopicId.orElse(0));
            query.setParameter("description", description.orElse(null));

            return query.list();

        }
    }

   /* @Override
    public List<Course> filter(FilterCourseParams fcp) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder query = new StringBuilder().append("from Course");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, String>();

            fcp.getCourseName().ifPresent(value -> {
                filters.add("title like :courseName");
                params.put("courseName", "%" + value + "%");
            });
            //ToDo How to add Integer to query
            fcp.getCourseTopicId().ifPresent(value -> {
                filters.add("topic = :courseTopicId");
                params.put("courseTopicId", String.valueOf(value));
            });

            fcp.getDescription().ifPresent(value -> {
                filters.add("description like :description");
                params.put("description", "%" + value + "%");
            });

//            fcp.getCourseRating().ifPresent(value -> {
//                filters.add("courseRating = :courseRating");
//                params.put("courseRating", String.valueOf(value));
//            });

            if (!filters.isEmpty()) {
                query.append(" where ").append(String.join(" and ", filters));
            }
            return session.createQuery(query.toString(), Course.class).setProperties(params).list();
        }
    }*/

    @Override
    public List<Course> sortByTitle() {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery("from Course c ORDER BY c.title DESC", Course.class);
            return query.list();
        }
    }
}




