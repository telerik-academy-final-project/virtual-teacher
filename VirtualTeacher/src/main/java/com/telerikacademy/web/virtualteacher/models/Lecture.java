package com.telerikacademy.web.virtualteacher.models;

import javax.persistence.*;

@Entity
@Table(name = "lectures")
public class Lecture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "video_path")
    private String videoPath;

    @OneToOne
    @JoinColumn(name = "assignment_id")
    private Assignment assignment;

    public Lecture() {
    }

    public Lecture(int id, String title, String description, String videoPath, Assignment assignment) {
        setId(id);
        setTitle(title);
        setDescription(description);
        setVideoPath(videoPath);
        setAssignment(assignment);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }
}
