package com.telerikacademy.web.virtualteacher.services.modelmappers;

import com.telerikacademy.web.virtualteacher.models.Course;
import com.telerikacademy.web.virtualteacher.models.dtos.CourseDto;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.CourseRepository;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.TopicRepository;
import org.springframework.stereotype.Component;

@Component
public class CourseModelMapper {
    private final CourseRepository courseRepository;
    private final TopicRepository topicRepository;

    public CourseModelMapper(CourseRepository courseRepository, TopicRepository topicRepository) {
        this.courseRepository = courseRepository;
        this.topicRepository = topicRepository;
    }

    public Course fromDto(CourseDto courseDto) {
        Course course = new Course();
        dtoToObject(courseDto, course);
        return course;
    }

    public Course fromDto(CourseDto courseDto, int id) {
        Course course = courseRepository.getById(id);
        dtoToObject(courseDto, course);
        return course;
    }


    private void dtoToObject(CourseDto courseDto, Course course) {
        course.setTitle(courseDto.getTitle());
        course.setDescription(courseDto.getDescription());
        course.setStartingDate(courseDto.getStartingDate());
        course.setTopic(topicRepository.getById(courseDto.getTopicId()));
    }
}
