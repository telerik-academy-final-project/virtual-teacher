package com.telerikacademy.web.virtualteacher.repositories;

import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.models.Lecture;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.LectureRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LectureRepositoryImpl implements LectureRepository {

    SessionFactory sessionFactory;

    public LectureRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Lecture> getAll(){
        try (Session session = sessionFactory.openSession()){
            Query<Lecture> query =session.createQuery("from Lecture ", Lecture.class);
            return query.list();
        }
    }

    @Override
    public Lecture getById(int id){
        try (Session session = sessionFactory.openSession()) {
            Lecture lecture = session.get(Lecture.class, id);
            if (lecture == null) {
                throw new EntityNotFoundException("Lecture", id);
            }
            return lecture;
        }
    }

    @Override
    public Lecture getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Lecture lecture = session.get(Lecture.class, name);
            if (lecture == null) {
                throw new EntityNotFoundException("Lecture", name, "");
            }
            return lecture;
        }
    }

    @Override
    public void delete(int id) {
        Lecture lectureToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(lectureToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Lecture lecture) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(lecture);
            session.getTransaction().commit();
        }
    }

    @Override
    public void create(Lecture lecture) {
        try (Session session = sessionFactory.openSession()) {
            session.save(lecture);
        }
    }
}
