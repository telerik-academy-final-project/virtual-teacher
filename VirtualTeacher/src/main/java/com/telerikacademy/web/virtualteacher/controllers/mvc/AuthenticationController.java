package com.telerikacademy.web.virtualteacher.controllers.mvc;

import com.telerikacademy.web.virtualteacher.controllers.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.models.dtos.LoginUserDto;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import com.telerikacademy.web.virtualteacher.services.modelmappers.UserModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;

    @Autowired
    public AuthenticationController(UserService userService, AuthenticationHelper authenticationHelper, UserModelMapper userModelMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginUserDto());
        return "login";
    }
}
