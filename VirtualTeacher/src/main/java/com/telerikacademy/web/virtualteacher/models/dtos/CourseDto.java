package com.telerikacademy.web.virtualteacher.models.dtos;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class CourseDto {
    @NotBlank
    @Size(min = 2, max = 50, message = "Title should be between 2 and 50 characters long")
    private String title;

    @Positive(message = "Topic Id must be a positive integer")
    private int topicId;

    @NotBlank
    @Size(min = 2, max = 1000, message = "Description should be between 2 and 1000 characters long")
    private String description;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startingDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }
}
