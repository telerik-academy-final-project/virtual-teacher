package com.telerikacademy.web.virtualteacher.services.interfaces;

import com.telerikacademy.web.virtualteacher.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAll();

    User getById(int id);

    void create(User user);

    void update(User user);

    void delete(int id, User user);

    List<User> search(Optional<String> email,
                      Optional<Integer> phone,
                      User user);

    User getByEmail(String email);

    void enroll(User user, int courseId);

    List<User> getAllTeachers();
}
