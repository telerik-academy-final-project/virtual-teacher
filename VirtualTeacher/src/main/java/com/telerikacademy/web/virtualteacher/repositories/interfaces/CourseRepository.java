package com.telerikacademy.web.virtualteacher.repositories.interfaces;

import com.telerikacademy.web.virtualteacher.models.Course;

import java.util.List;
import java.util.Optional;

public interface CourseRepository {
    List<Course> getAll();


    Course getById(int id);

    Course getByName(String name);

    void delete(int id);

    void update(Course course);

    void create(Course course);

    List<Course> filter(Optional<String> courseName,
                        Optional<Integer> courseTopicId,
                        Optional<String> description);

    List<Course> sortByTitle();
}
