package com.telerikacademy.web.virtualteacher.models.dtos;

import javax.validation.constraints.*;

public class UpdateUserDto {

    private static final String ROLE_MESSAGE_PICK_BETWEEN_1_AND_3 = "Pick between 1 and 3!";

    @NotBlank(message = "First name can not be empty")
    @Size(min = 2, max = 15, message = "First name should be between 2 and 20 characters long.")
    private String firstName;

    @NotBlank(message = "Last name can not be empty")
    @Size(min = 2, max = 15, message = "Last name should be between 2 and 20 characters long.")
    private String lastName;

    @Email(message = "Invalid email address")
    private String email;

    //@NotBlank(message = "Phone can not be empty")
    //@Size(min = 8, max = 15, message = "Phone should be between 8 and 15 numbers long.")
    @Positive
    private long phone;

    @Min(value = 1, message = ROLE_MESSAGE_PICK_BETWEEN_1_AND_3)
    @Max(value = 3, message = ROLE_MESSAGE_PICK_BETWEEN_1_AND_3)
    private int roleId;

    @NotBlank
    @Size(min = 8, max = 20, message = "Password should be between 8 and 20 characters long.")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!?])(?=\\S+$).{8,}$")
    private String password;

    @NotNull
    private String imgPath;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }
}
