package com.telerikacademy.web.virtualteacher.models.dtos;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class LectureDto {
    @NotNull
    @Length(min = 2, max = 50, message = "Title must be between 5 and 50 characters long")
    private String title;

    @NotNull
    @Length(min = 2, max = 1000, message = "Description should be between 2 and 1000 characters long")
    private String description;

    @NotNull
    private String videoPath;

    public LectureDto() {
    }

    public LectureDto(String title, String description, String videoPath) {
        setTitle(title);
        setDescription(description);
        setVideoPath(videoPath);

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

}
