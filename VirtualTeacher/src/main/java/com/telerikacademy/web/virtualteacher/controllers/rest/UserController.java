package com.telerikacademy.web.virtualteacher.controllers.rest;

import com.telerikacademy.web.virtualteacher.controllers.AuthenticationHelper;
import com.telerikacademy.web.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.web.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.web.virtualteacher.exceptions.RequiredRegistrationInformationException;
import com.telerikacademy.web.virtualteacher.models.User;
import com.telerikacademy.web.virtualteacher.models.dtos.ApprovalUserTeacherDto;
import com.telerikacademy.web.virtualteacher.models.dtos.RegisterUserDto;
import com.telerikacademy.web.virtualteacher.models.dtos.UpdateUserDto;
import com.telerikacademy.web.virtualteacher.services.interfaces.UserService;
import com.telerikacademy.web.virtualteacher.services.modelmappers.UserModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;
    private final UserModelMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService, UserModelMapper mapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        try {
            return userService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/teachers")
    public List<User> getAllTeachers() {
        return userService.getAllTeachers();
    }




    @PostMapping
    public User create(@Valid @RequestBody RegisterUserDto userDto) {
        try {
            var user = mapper.fromDto(userDto);
            if (!userDto.getPassword().equals(userDto.getPasswordConfirm())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Password and PasswordConfirm must be the same!");
            }
            userService.create(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (RequiredRegistrationInformationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}/enroll/{courseId}")
    public User enrollToCourse(@PathVariable int id, @PathVariable int courseId) {
        try {
            var userToEnroll = userService.getById(id);
            userService.enroll(userToEnroll, courseId);
            return userToEnroll;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id, @Valid @RequestBody UpdateUserDto userDto) {
        try {
            //User user2 = authenticationHelper.tryGetUser(headers);
            User user = mapper.fromDto(userDto, id);
            userService.update(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (RequiredRegistrationInformationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<User> search(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> email,
                             @RequestParam(required = false) Optional<Integer> phone) {

        User user = authenticationHelper.tryGetUser(headers);
        return userService.search(email, phone, user);

    }

    @PutMapping("/approve/teacher/{id}")
    public User update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id, @Valid @RequestBody ApprovalUserTeacherDto approvalUserTeacherDto) {
        try {
            //User userHeader = authenticationHelper.tryGetUser(headers);
            User user = mapper.fromDto(approvalUserTeacherDto, id);
            userService.update(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (RequiredRegistrationInformationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
