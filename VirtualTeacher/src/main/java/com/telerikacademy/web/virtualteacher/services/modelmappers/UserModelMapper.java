package com.telerikacademy.web.virtualteacher.services.modelmappers;

import com.telerikacademy.web.virtualteacher.models.Role;
import com.telerikacademy.web.virtualteacher.models.User;
import com.telerikacademy.web.virtualteacher.models.dtos.ApprovalUserTeacherDto;
import com.telerikacademy.web.virtualteacher.models.dtos.RegisterUserDto;
import com.telerikacademy.web.virtualteacher.models.dtos.UpdateUserDto;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.RoleRepository;
import com.telerikacademy.web.virtualteacher.repositories.interfaces.UserRepository;
import com.telerikacademy.web.virtualteacher.services.interfaces.RoleService;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class UserModelMapper {
    private final RoleRepository roleRepository;
    private final RoleService roleService;
    private final UserRepository userRepository;

    public UserModelMapper(RoleRepository roleRepository, RoleService roleService, UserRepository userRepository) {
        this.roleRepository = roleRepository;
        this.roleService = roleService;
        this.userRepository = userRepository;
    }

    public User fromDto(RegisterUserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(UpdateUserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(ApprovalUserTeacherDto approvalUserTeacherDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(approvalUserTeacherDto, user);
        return user;
    }


    private void dtoToObject(RegisterUserDto userDto, User user) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPhone(userDto.getPhone());
        user.setImgPath(userDto.getImgPath());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPasswordConfirm());
        Set<Role> roles = new HashSet<>();
        roles.add(roleService.getById(userDto.getRoleId()));
        user.setRoles(roles);
    }

    private void dtoToObject(UpdateUserDto userDto, User user) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPhone(userDto.getPhone());
        user.setImgPath(userDto.getImgPath());
        user.setPassword(userDto.getPassword());
        if (user.getRoles().stream().noneMatch(role -> role.getId() == userDto.getRoleId())) {
            user.getRoles().add(roleService.getById(userDto.getRoleId()));
        }
    }

    private void dtoToObject(ApprovalUserTeacherDto approvalUserTeacherDto, User user) {
        user.setEmail(approvalUserTeacherDto.getEmail());
        user.setApproved(approvalUserTeacherDto.isApproved());

    }
}
