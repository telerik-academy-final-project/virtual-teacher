package com.telerikacademy.web.virtualteacher.services;


import com.telerikacademy.web.virtualteacher.models.Role;
import com.telerikacademy.web.virtualteacher.repositories.RoleRepositoryImpl;
import com.telerikacademy.web.virtualteacher.services.interfaces.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepositoryImpl roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepositoryImpl roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.getAll();
    }

    @Override
    public Role getById(int id) {
        return roleRepository.getById(id);
    }

}
