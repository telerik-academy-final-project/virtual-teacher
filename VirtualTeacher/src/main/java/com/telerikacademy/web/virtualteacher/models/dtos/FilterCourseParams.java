package com.telerikacademy.web.virtualteacher.models.dtos;

import java.util.Optional;

public class FilterCourseParams {
    private Optional<String> courseName;
    private Optional<Integer> courseTopicId;
    private Optional<String> description;
    private Optional<Double> courseRating;

    public FilterCourseParams() {
    }

    public Optional<String> getCourseName() {
        return courseName;
    }

    public FilterCourseParams setCourseName(String courseName) {
        this.courseName = Optional.ofNullable(courseName);
        return this;
    }

    public Optional<Integer> getCourseTopicId() {
        return courseTopicId;
    }

    public FilterCourseParams setCourseTopicId(Integer courseTopicId) {
        this.courseTopicId = Optional.ofNullable(courseTopicId);
        return this;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public FilterCourseParams setDescription(String description) {
        this.description = Optional.ofNullable(description);
        return this;
    }

    public Optional<Double> getCourseRating() {
        return courseRating;
    }

    public FilterCourseParams setCourseRating(Double courseRating) {
        this.courseRating = Optional.ofNullable(courseRating);
        return this;
    }
}
