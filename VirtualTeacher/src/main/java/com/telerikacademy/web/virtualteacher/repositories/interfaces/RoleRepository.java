package com.telerikacademy.web.virtualteacher.repositories.interfaces;

import com.telerikacademy.web.virtualteacher.models.Role;

import java.util.List;

public interface RoleRepository {
    Role getById(int id);

    List<Role> getAll();
}
