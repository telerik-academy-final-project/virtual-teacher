package com.telerikacademy.web.virtualteacher.repositories.interfaces;

import com.telerikacademy.web.virtualteacher.models.Topic;

import java.util.List;

public interface TopicRepository {
    List<Topic> getAll();

    Topic getById(int id);

    Topic getByName(String name);
}
